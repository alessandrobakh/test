<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\ImageMeta;
use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AppFixtures extends Fixture
{

    /**
     * @var EncoderFactoryInterface
     */
    private EncoderFactoryInterface $encoderFactory;

    /**
     * @param EncoderFactoryInterface $encoderFactory
     */
    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('en_EN');

        $admin = new Admin();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setEmail('admin@admin.com');
        $admin->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('password', null));
        $manager->persist($admin);

        for ($i = 0; $i < 100000; $i++) {
            $product = new Product();
            $product->setName($faker->name);
            $product->setDescription($faker->realText(300));
            $product->setPrice($faker->randomFloat(2, 50, 100));
            $manager->persist($product);

            $productImage = new ProductImage();
            $productImage->setDescription($faker->realText(300));
            $productImage->setPicture('public/uploads/1.jpeg');
            $productImage->setProduct($product);
            $manager->persist($productImage);

            $imageMeta = new ImageMeta();
            $imageMeta->setTag($faker->word);
            $imageMeta->setProductImage($productImage);
            $manager->persist($imageMeta);
        }

        $manager->flush();
    }
}
