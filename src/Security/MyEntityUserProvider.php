<?php

namespace App\Security;

use App\Entity\User;
use HWI\Bundle\OAuthBundle\Connect\AccountConnectorInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\EntityUserProvider;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

class MyEntityUserProvider extends EntityUserProvider implements AccountConnectorInterface
{
    /**
     * @param UserResponseInterface $response
     * @return UserInterface|User
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response): UserInterface|User
    {
        $resourceOwnerName = $response->getResourceOwner()->getName();

        if (!isset($this->properties[$resourceOwnerName])) {
            throw new \RuntimeException(sprintf("No property defined for entity for resource owner '%s'.", $resourceOwnerName));
        }

        $serviceName = $response->getResourceOwner()->getName();
        $setterId = 'set'. ucfirst($serviceName). 'ID';
        $setterAccessToken = 'set'. ucfirst($serviceName). 'AccessToken';

        $username = $response->getUsername();
        if (null === $user = $this->findUser([$this->properties[$resourceOwnerName] => $username])) {
            $user = new User();
            $user->setEmail($response->getEmail());
            $user->setRoles(["ROLE_USER"]);
            $user->$setterId($username);
            $user->$setterAccessToken($response->getAccessToken());

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        }

        $user->$setterAccessToken($response->getAccessToken());

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param UserResponseInterface $response
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        if (!$user instanceof User){
            throw new UnsupportedUserException('Expected an instance of App\Entity\User, but got "%s".', get_class($user));
        }
    }
}