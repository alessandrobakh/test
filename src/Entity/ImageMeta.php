<?php

namespace App\Entity;

use App\Repository\ImageMetaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImageMetaRepository::class)
 */
class ImageMeta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity=ProductImage::class, inversedBy="metas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $productImage;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return ProductImage|null
     */
    public function getProductImage(): ?ProductImage
    {
        return $this->productImage;
    }

    /**
     * @param ProductImage|null $productImage
     * @return $this
     */
    public function setProductImage(?ProductImage $productImage): self
    {
        $this->productImage = $productImage;

        return $this;
    }
}
