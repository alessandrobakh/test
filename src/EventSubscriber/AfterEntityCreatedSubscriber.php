<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class AfterEntityCreatedSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $event
     * @return void
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function onAfterEntityPersisted($event)
    {
        $article = $event->getEntityInstance();
        $message = (new Email())
            ->from('mailer.symfony.testing@gmail.com')
            ->to('mailer.symfony.testing@gmail.com')
            ->subject('Created article: ' . $article->getTitle())
            ->text('New article created!',
                'text/plain');
        $this->mailer->send($message);
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            AfterEntityPersistedEvent::class => 'onAfterEntityPersisted'
        ];
    }
}
