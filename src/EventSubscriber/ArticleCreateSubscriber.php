<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\ArticleCreateEvent;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ArticleCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param ArticleCreateEvent $event
     * @return void
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function onArticleCreate(ArticleCreateEvent $event)
    {
        $article = $event->getArticle();
        $message = (new Email())
            ->from('mailer.symfony.testing@gmail.com')
            ->to('mailer.symfony.testing@gmail.com')
            ->subject('Created article: ' . $article->getTitle())
            ->text('New article created!',
                'text/plain');
        $this->mailer->send($message);
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ArticleCreateEvent::class => 'onArticleCreate'
        ];
    }
}
