<?php

namespace App\Command;

use App\Repository\ArticleRepository;
use App\Repository\EmailRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

#[AsCommand(
    name: 'app:article:send',
    description: 'Send all articles to all emails',
)]
class ArticleSendCommand extends Command
{

    /**
     * @var ArticleRepository
     */
    private ArticleRepository $articleRepository;
    /**
     * @var EmailRepository
     */
    private EmailRepository $emailRepository;
    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @param ArticleRepository $articleRepository
     * @param EmailRepository $emailRepository
     * @param MailerInterface $mailer
     */
    public function __construct(ArticleRepository $articleRepository, EmailRepository $emailRepository, MailerInterface $mailer)
    {
        parent::__construct();

        $this->articleRepository = $articleRepository;
        $this->emailRepository = $emailRepository;
        $this->mailer = $mailer;

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $articleData = $this->articleRepository->findAll();
        $emailData = $this->emailRepository->findAll();

        foreach ($emailData as $one_mail) {
            foreach ($articleData as $one_article) {
                $message = (new Email())
                    ->from('mailer.symfony.testing@gmail.com')
                    ->to($one_mail->getEmail())
                    ->subject($one_article->getTitle())
                    ->text($one_article->getBody(),
                        'text/plain');
                $this->mailer->send($message);
            }
        }

        $io->success('All articles were sent to all emails');

        return Command::SUCCESS;
    }
}
