<?php

namespace App\Event;

use App\Entity\Article;
use Symfony\Contracts\EventDispatcher\Event;

class ArticleCreateEvent extends Event
{
    public const NAME = 'article.create';

    /**
     * @var Article
     */
    protected Article $article;

    /**
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('Article ID: %s', $this->article->getId());
    }
}