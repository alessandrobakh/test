<?php

namespace App\Controller;

use App\Entity\Email;
use App\Form\EmailType;
use App\Repository\EmailRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class EmailController extends AbstractController
{
    /**
     * @param Environment $twig
     * @param EmailRepository $emailRepository
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/email", name="email")
     */
    public function index(Environment $twig, EmailRepository $emailRepository): Response
    {
        return new Response($twig->render('email/index.html.twig',
            ['emails' => $emailRepository->findAll()]));
    }


    /**
     * @param Request $request
     * @param Environment $twig
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/create-email", name="create-email")
     */
    public function create(Request $request, Environment $twig, EntityManagerInterface $entityManager): RedirectResponse|Response
    {
        $email = new Email();
        $form = $this->createForm(EmailType::class, $email);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($email);
            $entityManager->flush();

            return $this->redirectToRoute('homepage');
        }

        return new Response($twig->render('email/create.html.twig',
            [
                'email_form' => $form->createView(),
            ]));
    }
}
