<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\EmailRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleCrudController extends AbstractCrudController
{
    /**
     * @return string
     */
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    /**
     * @param string $pageName
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title')->setRequired(true),
            TextAreaField::new('body')->setRequired(true),
            DateTimeField::new('publish_at')->setRequired(true),
            Field::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms(),
            DateTimeField::new('updated_at')->hideOnForm(),
            ImageField::new('image')->setBasePath('/uploads/')->hideOnForm(),
        ];

    }

    /**
     * @param Actions $actions
     * @return Actions
     */
    public function configureActions(Actions $actions): Actions
    {
        $sensArticleToEmails = Action::new('sendArticleToEmailsAction', 'Send to emails', 'far fa-newspaper')
            ->linkToCrudAction('sendArticleToEmailsAction');

        return $actions
            ->add(Crud::PAGE_DETAIL, $sensArticleToEmails)
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }


    /**
     * @param ArticleRepository $articleRepository
     * @param EmailRepository $emailRepository
     * @param MailerInterface $mailer
     * @param Request $request
     * @return RedirectResponse
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @Route ("/sendArticleToEmailsAction/{$id}", "sendArticleToEmailsAction")
     */
    public function sendArticleToEmailsAction(ArticleRepository $articleRepository,
                                              EmailRepository   $emailRepository,
                                              MailerInterface   $mailer,
                                              Request           $request): RedirectResponse
    {
        $articleData = $articleRepository->find($request->get('entityId'));
        $emailData = $emailRepository->findAll();

        foreach ($emailData as $one_mail) {
            $message = (new Email())
                ->from('mailer.symfony.testing@gmail.com')
                ->to($one_mail->getEmail())
                ->subject($articleData->getTitle())
                ->text($articleData->getBody(),
                    'text/plain');
            $mailer->send($message);
        }


        return $this->redirectToRoute('admin');
    }

}
