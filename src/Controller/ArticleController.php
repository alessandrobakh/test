<?php

namespace App\Controller;

use App\Entity\Article;
use App\Event\ArticleCreateEvent;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMXPath;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ArticleController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Environment $twig
     * @param ArticleRepository $articleRepository
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/", name="homepage")
     */
    public function index(Environment $twig, ArticleRepository $articleRepository): Response
    {
        return new Response($twig->render('article/index.html.twig',
            [
                'articles' => $articleRepository->findAll(),
                'exchange_rates' => $this->getExchangeRates(),
            ]));
    }

    /**
     * @param Environment $twig
     * @param ArticleRepository $articleRepository
     * @param $id
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/article/{id}", name="article.show")
     */
    public function show(Environment $twig, ArticleRepository $articleRepository, $id): Response
    {
        return new Response($twig->render('article/show.html.twig', [
            'article' => $articleRepository->find($id),
        ]));
    }

    /**
     * @param Request $request
     * @param Environment $twig
     * @param EventDispatcherInterface $dispatcher
     * @return RedirectResponse|Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/create", name="article.create")
     */
    public function create(Request $request, Environment $twig, EventDispatcherInterface $dispatcher): RedirectResponse|Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($article);
            $this->entityManager->flush();

            $event = new ArticleCreateEvent($article);
            $dispatcher->dispatch($event, ArticleCreateEvent::class);

            return $this->redirectToRoute('homepage');
        }

        return new Response($twig->render('article/create.html.twig',
            [
                'article_form' => $form->createView(),
            ]));
    }


    /**
     * @return array|void
     */
    public function getExchangeRates()
    {
        libxml_use_internal_errors(true);
        $dom = new DomDocument;
        $dom->loadHTMLFile("https://www.nbkr.kg/index.jsp?lang=RUS");
        $xpath = new DomXPath($dom);
        $elements = $xpath->query("//div[@id='sticker-exrates']//table/tbody/tr");
        header("Content-type: text/plain");

        if (!is_null($elements)) {
            $arrayWithNodes = [];
            $i = 0;
            foreach ($elements as $element) {

                $nodes = $element->childNodes;
                $arrayWithNodes[$i] = [];
                foreach ($nodes as $node) {
                    if (!trim($node->nodeValue)) {
                        continue;
                    }
                    $arrayWithNodes[$i][] .= trim($node->nodeValue);
                }
                $i++;
            }

            $result = [];
            foreach ($arrayWithNodes as $item) {
                $result[] = implode(',', $item);
            }

            return $result;
        }
    }
}
