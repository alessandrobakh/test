<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @return Response
     * @Route ("/about", name="about")
     */
    public function getPageAbout(): Response
    {
        return $this->render('pages/about.html.twig');
    }

    /**
     * @return Response
     * @Route ("/contacts", name="contacts")
     */
    public function getPageContacts(): Response
    {
        return $this->render('pages/contacts.html.twig');
    }
}
