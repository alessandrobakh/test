<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;


class ProductController extends AbstractController
{
    /**
     * @var PaginatedFinderInterface
     */
    private PaginatedFinderInterface $finder;

    /**
     * @param PaginatedFinderInterface $finder
     */
    public function __construct(PaginatedFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param Request $request
     * @param Environment $twig
     * @param ProductRepository $productRepository
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/product", name="product")
     */
    public function index(Request $request, Environment $twig, ProductRepository $productRepository): Response
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $paginator = $productRepository->getProductPaginator($offset);
        $pagesArray = [];

        for ($i = 0; $i < round(count($paginator) / ProductRepository::PAGINATOR_PER_PAGE); $i++) {
            $pagesArray[] += $i;
        }

        return new Response($twig->render('product/index.html.twig',
            [
                'products' => $paginator,
                'previous' => $offset - ProductRepository::PAGINATOR_PER_PAGE,
                'next' => min(count($paginator), $offset + ProductRepository::PAGINATOR_PER_PAGE),
                'pages' => $pagesArray,
                'per_page' => ProductRepository::PAGINATOR_PER_PAGE,
            ]));
    }

    /**
     * @param Request $request
     * @param Environment $twig
     * @param ProductRepository $productRepository
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/search", name="search")
     */
    public function searchProductsDefault(Request $request, Environment $twig, ProductRepository $productRepository): Response
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $paginator = $productRepository->searchProducts($request->get('value'), $offset);

        $pagesArray = [];

        for ($i = 0; $i < round(count($paginator) / ProductRepository::PAGINATOR_PER_PAGE); $i++) {
            $pagesArray[] += $i;
        }

        return new Response($twig->render('product/search.html.twig',
            [
                'products' => $paginator,
                'previous' => $offset - ProductRepository::PAGINATOR_PER_PAGE,
                'next' => min(count($paginator), $offset + ProductRepository::PAGINATOR_PER_PAGE),
                'pages' => $pagesArray,
                'per_page' => ProductRepository::PAGINATOR_PER_PAGE,
                'value' => $request->get('value')
            ]));
    }

    /**
     * @param Environment $twig
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route ("/elastic-search", name="elastic-search")
     */
    public function searchProductsElastic(Environment $twig, PaginatorInterface $paginator, Request $request): Response
    {
        $results = $this->finder->find('%Eli%', 10000);

        $pagination = $paginator->paginate($results,
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/);

        return new Response($twig->render('product/elastic_search.html.twig',
            [
                'pagination' => $pagination,
            ]));
    }
}
